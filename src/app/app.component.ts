import { Component } from '@angular/core';
import {DataSourceService} from './data-source.service';
import {VirtualServer} from './models/virtual-server';
import {ignore} from 'selenium-webdriver/testing';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public virtualServers: VirtualServer[] = [];
  public totalUsageTime: string = null;
  public currentDateTime: Date = null;
  private totalUsageTimeIncrementing: boolean;

  constructor(private dataSource: DataSourceService) {
    this.dataSource.virtualServersState.subscribe(newState => {
      this.currentDateTime = new Date(Date.parse(newState.currentDateTime.toString()));
      this.virtualServers = newState.virtualServers.map<VirtualServer>(s => Object.assign(new VirtualServer(), s));
      this.totalUsageTime = newState.totalUsageTime;
      this.totalUsageTimeIncrementing = newState.totalUsageTimeIncrementing;
    });
    window.setInterval(() => {
      this.currentDateTime = new Date(this.currentDateTime.valueOf() + 1000);
      if (this.totalUsageTimeIncrementing) {
        const totalUsageDateTime = new Date(Date.parse('0001-01-01 ' + this.totalUsageTime) + 1000);
        const h = totalUsageDateTime.getHours();
        const m = totalUsageDateTime.getMinutes();
        const s = totalUsageDateTime.getSeconds();
        this.totalUsageTime = (h < 10 ? '0' + h : h)
          + ':' + (m < 10 ? '0' + m : m)
          + ':' + (s < 10 ? '0' + s : s);
      }
    }, 1000);
    this.dataSource.refresh();
  }

  public createNew() {
    this.dataSource.createNew();
  }

  public removeSelected() {
    if (this.haveNothingToRemove()) {
      return;
    }
    this.dataSource.remove(this.virtualServers.filter(vs => vs.selected).map(vs => vs.virtualServerId));
  }

  public refresh(): void {
    this.dataSource.refresh();
  }

  public haveNothingToRemove(): boolean {
    return this.virtualServers.filter(vs => vs.selected).length === 0;
  }
}
