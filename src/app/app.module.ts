import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { IsoDateTimePipe } from './iso-date-time.pipe';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TimeSpanPipe } from './time-span.pipe';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    IsoDateTimePipe,
    TimeSpanPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
