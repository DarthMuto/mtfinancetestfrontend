export class VirtualServer {
  public virtualServerId: number;
  public createDateTime: Date;
  public removeDateTime: Date;
  public selected = false;
}
