import {VirtualServer} from './virtual-server';

export class VirtualServersSummary {
  public currentDateTime: Date;
  public totalUsageTime: string;
  public virtualServers: VirtualServer[];
  public totalUsageTimeIncrementing: boolean;
}
