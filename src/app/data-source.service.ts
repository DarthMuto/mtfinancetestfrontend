import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject, throwError} from 'rxjs';
import {VirtualServersSummary} from './models/virtual-servers-summary';
import {HttpClient} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataSourceService {

  // private endpointUrl = 'http://localhost:5000/api/virtual-servers';
  private endpointUrl = '/api/virtual-servers';

  public virtualServersState: Subject<VirtualServersSummary> = new Subject<VirtualServersSummary>();

  public constructor(private httpClient: HttpClient) {}

  private nextState(state: VirtualServersSummary): void {
    this.virtualServersState.next(state);
  }

  public refresh(): void {
    this.httpClient
      .get<VirtualServersSummary>(this.endpointUrl)
      .pipe(catchError(e => this.catchError(e)))
      .subscribe(s => this.nextState(s));
  }

  public createNew(): void {
    this.httpClient
      .post<VirtualServersSummary>(this.endpointUrl, '')
      .pipe(catchError(e => this.catchError(e)))
      .subscribe(s => this.nextState(s));
  }

  public remove(idsToRemove: number[]): void {
    this.httpClient
      .post<VirtualServersSummary>(this.endpointUrl + '/remove', {idsToRemove})
      .pipe(catchError(e => this.catchError(e)))
      .subscribe(s => this.nextState(s));
  }

  private catchError(err) {
    const msg = 'Failed to connect to ' + this.endpointUrl;
    alert(msg);
    return throwError(msg);
  }
}
