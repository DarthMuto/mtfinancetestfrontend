import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeSpan'
})
export class TimeSpanPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    if (value === null) {
      return '';
    }
    const valueArr = value.split('.');
    if (valueArr.length > 1) {
      valueArr.pop();
    }
    return valueArr.join('.');
  }

}
